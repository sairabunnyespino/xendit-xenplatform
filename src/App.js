import CreditCard from './component/Xendit/CreditCard/';

function App() {
  return (
    <div className="App">
      <CreditCard />
    </div>
  );
}

export default App;
