const getExpiryDateTrim = (expiry, isYear=false) => {
    if (isYear) {
        return expiry.substring(5,7);
    }
    return expiry.substring(0,2);
}


export const getExpiryYear = expiry => {

    const YY = getExpiryDateTrim(expiry, true);
    const today = new Date();
    const today_year = today.getFullYear();
    const today_year_string = today_year.toString()
    const year_trim = today_year_string.substring(0, 2);
    const final_year = year_trim + YY;
    // const expiry_year = parseInt(final_year);
    return final_year;
}


export const getExpiryMonth = expiry => {
    const month = expiry.substring(0,2);
    return month;
}
