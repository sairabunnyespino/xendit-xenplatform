
    import styled from 'styled-components'

    export const CreditCardWrapper = styled.div`
        .container {
            text-align: center;
            margin-top: 10px;
        }

        .credit-card-div {
            border: 1px solid rgb(228, 228, 288);
        }
    `