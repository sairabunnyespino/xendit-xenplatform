import React, { useState } from 'react';
import { CreditCardWrapper } from './style';
import CreditCardInput from 'react-credit-card-input';
import { Button, Modal } from 'react-bootstrap';
import { getExpiryYear, getExpiryMonth } from './utils';
import 'bootstrap/dist/css/bootstrap.min.css';

const XenditCreditCard = () => {

    const [ cardDetails, setCardDetails ] = useState({
        cvc: '',
        expiry: '',
        card_no: ''
    });
    const [ authenticationUrl, setAuthenticalUrl ] = useState(null);
    const [ showModal, setShowModal ] = useState(false);

    const handleClose = () => setShowModal(false);
    console.log('authenticationUrl', authenticationUrl)
    //Modal for 3DS authentication
    const AuthenticationModal = (
        <Modal show={showModal} onHide={handleClose}>
            <Modal.Body>
            <iframe
                src={authenticationUrl}
                height="353"
                width="358"
                id="sample-inline-frame"
                name="sample-inline-frame"
                title="xendit"
                frameBorder="0"
            ></iframe>
            </Modal.Body>
        </Modal>
    )
    // handle the new value of CVV
    const CVVonChange = e => {
        let newCardDetails = {
            ...cardDetails
        }
        newCardDetails.cvc = e.target.value;
        setCardDetails(newCardDetails);
    };
    // handle the new value of Card Number
    const cardNumberOnChange = e => {
        let newCardDetails = {
            ...cardDetails
        }
        newCardDetails.card_no = e.target.value;
        setCardDetails(newCardDetails);
    };
    // handle the new value of expiry
    const expiryOnChange = e => {
        let newCardDetails = {
            ...cardDetails
        }
        newCardDetails.expiry = e.target.value;
        setCardDetails(newCardDetails);
    };
    // validates if all card details 
    const validation = () => {
        const { card_no, cvc, expiry } = cardDetails;
        if (card_no && cvc && expiry) {
            return true;
        }
        return false;
    };
    // xendit callback handler 
    const xenditResponseHandler = (err, token) => {
        if (err || !token) {
            console.log('status1: ', false);
            console.log('err', err);
        } else if (token.status === 'IN_REVIEW') {
            // get the url
            setAuthenticalUrl(token.payer_authentication_url);
            //open the modal
            setShowModal(true);
        } else if (token.status === 'VERIFIED') {
            console.log('status: ', token.status);
            console.log('token', token);
            setShowModal(false);
            setAuthenticalUrl(null);
        }else {
            console.log('status: ', false);
            console.log('token', token);
        }
    };
    // handle the request of tokenisation 
    const handleTokenization = () => {
        if (validation()) {
            const year = getExpiryYear(cardDetails.expiry);
            const month = getExpiryMonth(cardDetails.expiry);
            let finalCardNumber = cardDetails.card_no.replace(/ /g, '');
          
            //first parameter is the credit card details
            //second parameter is the xendit callback
            const creditCardDetails = {
                amount: 500,
                card_number: finalCardNumber,
                card_exp_month: month,
                card_exp_year: year,
                currency: 'php',
                card_cvn: cardDetails.cvn,
                is_multiple_use: false,
                should_authenticate: true,
                on_behalf_of: '60a8e38cd6641740af545889'
            };     
            window.Xendit.card.createToken(creditCardDetails, xenditResponseHandler);
        } else return console.log('error', cardDetails)
    }
  
    return (
       <CreditCardWrapper>
            <div className="container">
               <div className="credit-card-div">
                    <CreditCardInput 
                        cardNumberInputProps={{ value: cardDetails.card_no, onChange: cardNumberOnChange }}
                        cardExpiryInputProps={{ value: cardDetails.expiry, onChange: expiryOnChange }}
                        cardCVCInputProps={{ value: cardDetails.cvc, onChange: CVVonChange }}
                        fieldClassName="input"
                    />
                     <div>
                        <Button variant="primary" onClick={handleTokenization}>Create Token</Button>
                    </div>
               </div>
            </div>
            {AuthenticationModal}
       </CreditCardWrapper>
    )
}

export default XenditCreditCard;