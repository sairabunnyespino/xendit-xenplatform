export const xenditResponseHandler = (err, token) => {
    if (err || !token) {
        return {
            status: false,
            error: err   
        }
    } else if (token.status === 'IN_REVIEW' || token.status === 'VERIFIED') {
        return {
            success: token.status,
            token   
        }
    } else {
        return {
            status: false,
            error: err   
        }
    }
  };
